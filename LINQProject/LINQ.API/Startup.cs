﻿using LINQ.BLL.Entities;
using LINQ.BLL.Interfaces;
using LINQ.BLL.Services;
using LINQ.DataAccess.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LINQ.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IService<Project>, Service<Project>>();
            services.AddScoped<IService<Task>, Service<Task>>();
            services.AddScoped<IService<TaskState>, Service<TaskState>>();
            services.AddScoped<IService<Team>, Service<Team>>();
            services.AddScoped<IService<User>, Service<User>>();

            services.AddCors(corsOptions => { corsOptions.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin()); });
            services.AddMvc(option => option.EnableEndpointRouting = false);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(options => options.AllowAnyOrigin());
            app.UseMvc();
        }
    }
}
