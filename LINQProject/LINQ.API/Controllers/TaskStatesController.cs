﻿using System;
using System.Collections.Generic;
using LINQ.BLL.Entities;
using LINQ.BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace LINQ.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStateController : ControllerBase
    {
        private IService<TaskState> service;

        public TaskStateController(IService<TaskState> service) =>
            this.service = service;

        // GET api/taskstates
        [HttpGet]
        public ActionResult<IEnumerable<TaskState>> Get() =>
            Ok(service.GetAll());

        // GET api/taskstates/5
        [HttpGet("{id}")]
        public ActionResult<Project> Get(int id)
        {
            try
            {
                return Ok(service.Get(id));
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        // POST api/taskstates
        [HttpPost]
        public void Post([FromBody] TaskState taskState)
        {
            try
            {
                service.Create(taskState);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        // PUT api/taskstates/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] TaskState taskState)
        {
            try
            {
                service.Update(id, taskState);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        // DELETE api/taskstates/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
