﻿using System;
using System.Collections.Generic;
using LINQ.BLL.Entities;
using LINQ.BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace LINQ.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private IService<Team> service;

        public TeamsController(IService<Team> service) =>
            this.service = service;

        // GET api/teams
        [HttpGet]
        public ActionResult<IEnumerable<Team>> Get() =>
            Ok(service.GetAll());

        // GET api/teams/5
        [HttpGet("{id}")]
        public ActionResult<Team> Get(int id)
        {
            try
            {
                return Ok(service.Get(id));
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        // POST api/teams
        [HttpPost]
        public void Post([FromBody] Team team)
        {
            try
            {
                service.Create(team);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        // PUT api/teams/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Team team)
        {
            try
            {
                service.Update(id, team);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        // DELETE api/teams/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
