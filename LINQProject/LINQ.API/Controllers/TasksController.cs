﻿using System;
using System.Collections.Generic;
using LINQ.BLL.Entities;
using LINQ.BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace LINQ.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private IService<Task> service;

        public TaskController(IService<Task> service) =>
            this.service = service;

        // GET api/tasks
        [HttpGet]
        public ActionResult<IEnumerable<Project>> Get() =>
            Ok(service.GetAll());

        // GET api/tasks/5
        [HttpGet("{id}")]
        public ActionResult<Project> Get(int id)
        {
            try
            {
                return Ok(service.Get(id));
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        // POST api/tasks
        [HttpPost]
        public void Post([FromBody] Task task)
        {
            try
            {
                service.Create(task);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        // PUT api/tasks/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Task task)
        {
            try
            {
                service.Update(id, task);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        // DELETE api/tasks/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
