﻿using System;
using System.Collections.Generic;
using LINQ.BLL.Entities;
using LINQ.BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace LINQ.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IService<User> service;

        public UsersController(IService<User> service) =>
            this.service = service;

        // GET api/users
        [HttpGet]
        public ActionResult<IEnumerable<User>> Get() =>
            Ok(service.GetAll());

        // GET api/users/5
        [HttpGet("{id}")]
        public ActionResult<User> Get(int id)
        {
            try
            {
                return Ok(service.Get(id));
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        // POST api/users
        [HttpPost]
        public void Post([FromBody] User user)
        {
            try
            {
                service.Create(user);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        // PUT api/users/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] User user)
        {
            try
            {
                service.Update(id, user);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
