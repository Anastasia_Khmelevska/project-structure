﻿using System;
using System.Collections.Generic;
using LINQ.BLL.Entities;
using LINQ.BLL.Services;
using Microsoft.AspNetCore.Mvc;

namespace LINQ.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private IService<Project> service;

        public ProjectController(IService<Project> service) =>
            this.service = service;

        // GET api/projects
        [HttpGet("~/api/projects")]
        public ActionResult<IEnumerable<Project>> Get() =>
            Ok(service.GetAll());

        // GET api/projects/5
        [HttpGet("{id}")]
        public ActionResult<Project> Get(int id)
        {
            try
            {
                return Ok(service.Get(id));
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        // POST api/projects
        [HttpPost]
        public void Post([FromBody] Project project)
        {
            try
            {
                service.Create(project);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        // PUT api/projects/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Project project)
        {
            try
            {
                service.Update(id, project);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        // DELETE api/projects/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
