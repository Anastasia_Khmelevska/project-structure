﻿using System;

namespace LINQProject.ConsoleInterfaces
{
    public class ConsoleInterface
    {
        #region Entry Point
        public void Run()
        {
            while (true)
            {
                var point = NavigationList("SELECT TASK", new string[]
                {
                    "Task 1",
                    "Task 2",
                    "Task 3",
                    "Task 4",
                    "Task 5",
                    "Task 6",
                    "Task 7",
                    "EXIT"
                });
                switch (point)
                {
                    case 0:
                        Task1();
                        continue;
                    case 1:
                        Task2();
                        continue;
                    case 2:
                        Task3();
                        continue;
                    case 3:
                        Task4();
                        continue;
                    case 4:
                        Task5();
                        continue;
                    case 5:
                        Task6();
                        continue;
                    case 6:
                        Task7();
                        continue;
                    default:
                        break;
                }
                break;
            }
        }
        #endregion
        #region Tasks
        private LINQExecutor executor;
        private LINQExecutor Executor =>
            executor ?? (executor = new LINQExecutor());
        private void Task1()
        {
            Console.Clear();
            Console.Write("\nENTER AUTHOR ID: ");
            var id = int.Parse(Console.ReadLine());
            try
            {
                var result = Executor.Task1(id);
                foreach (var keyValue in result)
                    Console.WriteLine("\tKey: " + keyValue.Key + " | Value: " + keyValue.Value);
            }
            catch (Exception exception) { Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine(exception.Message); Console.ResetColor(); }
            Console.ReadKey();
        }
        private void Task2()
        {
            Console.Clear();
            Console.Write("\nENTER PERFORMER ID: ");
            var id = int.Parse(Console.ReadLine());
            try
            {
                var result = Executor.Task2(id);
                foreach (var task in result)
                    Console.WriteLine("\t" + task.ToString());
            }
            catch (Exception exception) { Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine(exception.Message); Console.ResetColor(); }
            Console.ReadKey();
        }
        private void Task3()
        {
            Console.Clear();
            Console.Write("\nENTER PERFORMER ID: ");
            var id = int.Parse(Console.ReadLine());
            try
            {
                var result = Executor.Task3(id);
                foreach (var res in result)
                    Console.WriteLine("\tId: " + res.Item1 + " | Name: " + res.Item2);
            }
            catch (Exception exception) { Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine(exception.Message); Console.ResetColor(); }
            Console.ReadKey();
        }
        private void Task4()
        {
            Console.Clear();
            try
            {
                var result = Executor.Task4();
                foreach (var res in result)
                {
                    Console.WriteLine("\tId: " + res.Item1 + " | Name: " + res.Item2);
                    foreach (var author in res.Item3)
                        Console.WriteLine("\t\tId: " + author.Id + " | Name: " + author.FirstName + " " + author.LastName + " | Registered at: " + author.RegisteredAt.ToString());
                }
            }
            catch (Exception exception) { Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine(exception.Message); Console.ResetColor(); }

            Console.ReadKey();
        }
        private void Task5()
        {
            Console.Clear();
            Console.WriteLine("Task 5 structure not valid, created 3 methods");
            Console.WriteLine("\nMETHOD 51 - return List of users ordered by FirstName");
            try
            {
                var result1 = Executor.Task51();
                foreach (var user in result1)
                    Console.WriteLine("\tId: " + user.Id + " | Name: " + user.FirstName + " " + user.LastName);
                Console.WriteLine("\nMETHOD 52 - return List of tasks ordered by Name lenght By Descending");
                var result2 = Executor.Task52();
                foreach (var task in result2)
                    Console.WriteLine("\tId: " + task.Id + " | Name: " + task.Name);
                Console.WriteLine("\nMETHOD 53 - return List of tasks ordered by Name lenght By Descending and with ordered Users by FirstName");
                var result3 = Executor.Task53();
                foreach (var task in result3)
                {
                    Console.WriteLine("\tId: " + task.Id + " | Name: " + task.Name);
                    Console.WriteLine("\t\tId: " + task.Performer.Id + " | Name: " + task.Performer.FirstName + " " + task.Performer.LastName);
                }
            }
            catch (Exception exception) { Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine(exception.Message); Console.ResetColor(); }
            Console.ReadKey();
        }
        private void Task6()
        {
            Console.Clear();
            Console.Write("\nENTER USER ID: ");
            var id = int.Parse(Console.ReadLine());
            try
            {
                var result = Executor.Task6(id);
                Console.WriteLine("\n\tAUTHOR| Id: " + result.Item1.Id + " | Name: " + result.Item1.FirstName + " " + result.Item1.LastName);
                Console.WriteLine("\tPROJECT| Id: " + result.Item2.Id + " | User Id: " + result.Item2.Author.Id + " | Created at: " + result.Item2.CreatedAt);
                Console.WriteLine("\tTASKS IN OLDEST PROJECT| Tasks: " + result.Item3);
                Console.WriteLine("\tLONGEST TASK| Id: " + result.Item4.Id + " | User Id: " + result.Item4.Performer.Id + " | Created at: " + result.Item4.CreatedAt.ToShortDateString() + " | Finished at: " + result.Item4.FinishedAt.ToShortDateString() + " | Lenght: " + (result.Item4.FinishedAt - result.Item4.CreatedAt).Days.ToString());
            }
            catch (Exception exception) { Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine(exception.Message); Console.ResetColor(); }
            Console.ReadKey();
        }
        private void Task7()
        {
            Console.Clear();
            Console.Write("\nENTER PROJECT ID: ");
            var id = int.Parse(Console.ReadLine());
            try
            {
                var result = Executor.Task7(id);
                Console.WriteLine("\n\tPROJECT| Id: " + result.Item1.Id + " | Name: " + result.Item1.Name);
                Console.WriteLine("\tLONGEST TASK| Id: " + result.Item2.Id + " | Created at: " + result.Item2.CreatedAt.ToShortDateString() + " | Finished at: " + result.Item2.FinishedAt.ToShortDateString() + " | Lenght: " + (result.Item2.FinishedAt - result.Item2.CreatedAt).Days.ToString());
                Console.WriteLine("\tSHORTEST TASK| Id: " + result.Item3.Id + " | Created at: " + result.Item3.CreatedAt.ToShortDateString() + " | Finished at: " + result.Item3.FinishedAt.ToShortDateString() + " | Lenght: " + (result.Item3.FinishedAt - result.Item3.CreatedAt).Days.ToString());
                Console.WriteLine("\tNUMBER OF AUTHORS| AUTHORS: " + result.Item4);
            }
            catch (Exception exception) { Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine(exception.Message); Console.ResetColor(); }

            Console.ReadKey();
        }
        #endregion
        #region Navigation
        private static int NavigationList(string title, string[] items, int stepAfter = -1, string pointer = "-->")
        {
            pointer = " " + pointer + " ";
            var step = new string(' ', pointer.Length);
            var active = 0;

            while (true)
            {
                Console.Clear();
                Console.WriteLine(step + title);
                for (var i = 0; i < items.Length; i++)
                {
                    Console.WriteLine((i == active ? pointer : step) + items[i]);
                    if (i == stepAfter + 1) Console.WriteLine();
                }
                var key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        active = active == 0 ? items.Length - 1 : active - 1;
                        continue;
                    case ConsoleKey.DownArrow:
                        active = active == items.Length - 1 ? 0 : active + 1;
                        continue;
                    case ConsoleKey.Enter:
                        break;
                    default:
                        continue;
                }
                return active;
            }
        }
        #endregion
    }
}
