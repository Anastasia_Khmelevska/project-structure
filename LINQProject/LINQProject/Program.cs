﻿using LINQProject.ConsoleInterfaces;

namespace LINQProject
{
    class Program
    {
        static void Main(string[] args) =>
            new ConsoleInterface().Run();
    }
}
