﻿using System;

namespace LINQProject.Models
{
    public class Team : Entity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
