﻿using System;
using System.Collections.Generic;

namespace LINQProject.Models
{
    public class Project : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
        public AuthorPerformer Author { get; set; }
        public Team Team { get; set; }
    }
}
