﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQProject.Models
{
    public class AuthorPerformer : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public long TeamId { get; set; }
    }
}
