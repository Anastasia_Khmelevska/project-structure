﻿namespace LINQProject.APIClients.APIModels
{
    public class TaskStateAPIModel : BaseModel
    {
        public string Value { get; set; }
    }
}
