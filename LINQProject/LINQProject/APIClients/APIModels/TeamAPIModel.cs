﻿using System;

namespace LINQProject.APIClients.APIModels
{
    public class TeamAPIModel : BaseModel
    {
        public string Name { get; set; }
        public DateTime Created_At { get; set; }
    }
}
