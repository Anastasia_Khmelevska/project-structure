﻿using System;

namespace LINQProject.APIClients.APIModels
{
    public class TaskAPIModel : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Finished_At { get; set; }
        public long State { get; set; }
        public long Project_Id { get; set; }
        public long Performer_Id { get; set; }
    }
}
