﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using LINQProject.APIClients.APIModels;

namespace LINQProject.APIClients
{
    public class APIClient
    {
        #region Properties
        public string BaseAddress { get; set; } = "http://localhost:5000/";
        #endregion
        #region Containers
        private IEnumerable<ProjectAPIModel> projects;
        private IEnumerable<TaskAPIModel> tasks;
        private IEnumerable<TaskStateAPIModel> taskStates;
        private IEnumerable<TeamAPIModel> teams;
        private IEnumerable<UserAPIModel> users;
        #endregion
        #region API Methods
        public ProjectAPIModel GetProject(long id)
        {
            var request = "api/Projects/" + id;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsAsync<ProjectAPIModel>().Result;
                }
                else
                {
                    throw new HttpRequestException("GetProjects/" + id);
                }
            }
        }
        public IEnumerable<ProjectAPIModel> GetProjects()
        {
            if (projects != null) return projects;

            var request = "api/Projects";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    projects = response.Content.ReadAsAsync<IEnumerable<ProjectAPIModel>>().Result;
                    return projects;
                }
                else
                {
                    throw new HttpRequestException("GetProjects");
                }
            }
        }

        public TaskAPIModel GetTask(long id)
        {
            var request = "api/Tasks/" + id;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsAsync<TaskAPIModel>().Result;
                }
                else
                {
                    throw new HttpRequestException("GetTasks/" + id);
                }
            }
        }
        public IEnumerable<TaskAPIModel> GetTasks()
        {
            if (tasks != null) return tasks;

            var request = "api/Tasks";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    tasks = response.Content.ReadAsAsync<IEnumerable<TaskAPIModel>>().Result;
                    return tasks;
                }
                else
                {
                    throw new HttpRequestException("GetTasks");
                }
            }
        }

        public IEnumerable<TaskStateAPIModel> GeTaskStates()
        {
            if (taskStates != null) return taskStates;

            var request = "api/TaskStates";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    taskStates = response.Content.ReadAsAsync<IEnumerable<TaskStateAPIModel>>().Result;
                    return taskStates;
                }
                else
                {
                    throw new HttpRequestException("GetTaskStates");
                }
            }
        }

        public TeamAPIModel GetTeam(long id)
        {
            var request = "api/Teams/" + id;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsAsync<TeamAPIModel>().Result;
                }
                else
                {
                    throw new HttpRequestException("GetTeams/" + id);
                }
            }
        }
        public IEnumerable<TeamAPIModel> GetTeams()
        {
            if (teams != null) return teams;

            var request = "api/Teams";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    teams = response.Content.ReadAsAsync<IEnumerable<TeamAPIModel>>().Result;
                    return teams;
                }
                else
                {
                    throw new HttpRequestException("GetTeams");
                }
            }
        }

        public UserAPIModel GetUser(long id)
        {
            var request = "api/Users/" + id;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsAsync<UserAPIModel>().Result;
                }
                else
                {
                    throw new HttpRequestException("GetUsers/" + id);
                }
            }
        }
        public IEnumerable<UserAPIModel> GetUsers()
        {
            if (users != null) return users;

            var request = "api/Users";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    users = response.Content.ReadAsAsync<IEnumerable<UserAPIModel>>().Result;
                    return users;
                }
                else
                {
                    throw new HttpRequestException("GetUsers");
                }
            }
        }
        #endregion
    }
}
