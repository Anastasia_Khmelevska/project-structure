﻿using System;
using System.Linq;
using System.Collections.Generic;
using LINQProject.Models;
using LINQProject.Services;

namespace LINQProject
{
    public class LINQExecutor
    {
        #region Properties
        private APIService service = new APIService();
        #endregion
        #region Tasks
        public Dictionary<int, int> Task1(int id) =>
            new Dictionary<int, int> {
                { id, service.GetProjects()
                    .Single(project => project.Author.Id == id)
                    .Tasks.Count() }
            };

        public List<Task> Task2(int id) =>
            service.GetTasks()
                .Where(task => task.Performer.Id == id)
                .Where(task => task.Name.Length < 45)
                .ToList();

        public List<(int, string)> Task3(int id) =>
            service.GetTasks()
                .Where(task => task.Performer.Id == id && task.FinishedAt < DateTime.Now)
                .Select(task => (task.Id, task.Name))
                .ToList();

        public List<(int, string, List<AuthorPerformer>)> Task4() =>
            service.GetTeams()
                .Select(team => (
                    team.Id,
                    team.Name,
                    service.GetAuthorPerformers()
                        .Where(player => DateTime.Now.Year - player.Birthday.Year > 12)
                        .OrderByDescending(player => player.RegisteredAt)
                        .ToList()
                )).ToList();

        // Task5 structure not valid, creating 3 methods
        // Method 51 - return List of users ordered by FirstName
        public List<AuthorPerformer> Task51() =>
            service.GetAuthorPerformers()
                .OrderBy(user => user.FirstName)
                .ToList();
        // Method 52 - return List of tasks ordered by Name lenght By Descending
        public List<Task> Task52() =>
            service.GetTasks()
                .OrderByDescending(task => task.Name.Length)
                .ToList();
        // method 53 - return List of tasks ordered by Name lenght By Descending and with ordered Users by FirstName
        public List<Task> Task53() =>
            service.GetTasks()
                .OrderByDescending(task => task.Name.Length)
                .OrderBy(task => task.Performer.FirstName)
                .ToList();

        public (AuthorPerformer, Project, int, Task) Task6(int id) =>
            (
                service.GetAuthorPerformers()
                    .Single(user => user.Id == id),
                service.GetProjects()
                    .Where(project => project.Author.Id == id)
                    .OrderByDescending(project => project.CreatedAt)
                    .First(),
                service.GetProjects()
                    .Where(project => project.Author.Id == id)
                    .OrderByDescending(project => project.CreatedAt)
                    .First()
                    .Tasks.Count(),
                service.GetTasks()
                    .Where(task => task.Performer.Id == id)
                    .OrderByDescending(task => task.FinishedAt - task.CreatedAt)
                    .First()
            );

        public (Project, Task, Task, int) Task7(int id) =>
            (
                service.GetProjects()
                    .Single(project => project.Id == id),
                service.GetTasks()
                    .Where(task => task.ProjectId == id)
                    .OrderByDescending(task => task.FinishedAt - task.CreatedAt)
                    .First(),
                service.GetTasks()
                    .Where(task => task.ProjectId == id)
                    .OrderBy(task => task.FinishedAt - task.CreatedAt)
                    .First(),
                service.GetProjects()
                    .Where(project => project.Description.Length > 25 || project.Tasks.Count() < 3)
                    .Select(project => project.Author)
                    .Count()
            );
        #endregion
    }
}
