﻿using System.Collections.Generic;
using LINQ.BLL.Entities;

namespace LINQ.BLL.Services
{
    public interface IService<T> where T : BaseEntity
    {
        T Get(int id);
        IEnumerable<T> GetAll();
        void Create(T entity);
        void Update(int id, T entity);
        void Delete(int id);
        void Delete(T entity);
    }
}
