﻿using System.Collections.Generic;
using LINQ.BLL.Entities;
using LINQ.BLL.Interfaces;

namespace LINQ.BLL.Services
{
    public class Service<T> : IService<T> where T : BaseEntity
    {
        #region Properties
        private readonly IRepository<T> Repository;
        #endregion
        #region Constructors
        // The program does not work with DB - Save-method not needed
        public Service(IUnitOfWork unitOfWork) =>
            Repository = unitOfWork.GetRepository<T>();
        #endregion
        #region Public Methods
        public T Get(int id) =>
            Repository.Get(id);
        public IEnumerable<T> GetAll() =>
            Repository.GetAll();
        public void Create(T entity) =>
            Repository.Add(entity);
        public void Update(int id, T entity) =>
            Repository.Update(id, entity);
        public void Delete(int id) =>
            Repository.Delete(id);
        public void Delete(T entity) =>
            Repository.Delete(entity.Id);
        #endregion
    }
}
