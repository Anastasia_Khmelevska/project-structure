﻿using System;

namespace LINQ.BLL.Entities
{
    public class Project : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Deadline { get; set; }
        public long Author_Id { get; set; }
        public long Team_Id { get; set; }
    }
}
