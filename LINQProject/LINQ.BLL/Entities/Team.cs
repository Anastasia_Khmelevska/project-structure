﻿using System;

namespace LINQ.BLL.Entities
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }
        public DateTime Created_At { get; set; }
    }
}
