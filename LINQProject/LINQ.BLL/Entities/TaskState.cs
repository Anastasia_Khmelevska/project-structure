﻿namespace LINQ.BLL.Entities
{
    public class TaskState : BaseEntity
    {
        public string Value { get; set; }
    }
}
