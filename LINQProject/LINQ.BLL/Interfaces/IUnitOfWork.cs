﻿using System;
using LINQ.BLL.Entities;

namespace LINQ.BLL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        #region Repositories
        IRepository<T> GetRepository<T>() where T : BaseEntity;
        #endregion
    }
}
