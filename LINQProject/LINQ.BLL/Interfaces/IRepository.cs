﻿using System.Collections.Generic;
using LINQ.BLL.Entities;

namespace LINQ.BLL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T Get(int id);
        IEnumerable<T> GetAll();
        T Add(T entity);
        T Update(int id, T Entity);
        void Delete(int id);
    }
}
