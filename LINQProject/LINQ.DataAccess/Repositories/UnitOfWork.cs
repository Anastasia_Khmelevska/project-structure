﻿using System;
using LINQ.BLL.Entities;
using LINQ.BLL.Interfaces;

namespace LINQ.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Repositories
        private Repository<Project> projectRepository;
        private Repository<Project> ProjectRepository =>
            projectRepository ?? (projectRepository = new ProjectRepository());

        private Repository<Task> taskRepository;
        private Repository<Task> TaskRepository =>
            taskRepository ?? (taskRepository = new TaskRepository());

        private Repository<TaskState> taskStateRepository;
        private Repository<TaskState> TaskStateRepository =>
            taskStateRepository ?? (taskStateRepository = new TaskStateRepository());

        private Repository<Team> teamRepository;
        private Repository<Team> TeamRepository =>
            teamRepository ?? (teamRepository = new TeamRepository());

        private Repository<User> userRepository;
        private Repository<User> UserRepository =>
            userRepository ?? (userRepository = new UserRepository());
        #endregion
        #region Public Methods
        public IRepository<T> GetRepository<T>() where T : BaseEntity
        {
            if (typeof(T).Name == typeof(Project).Name)
                return (IRepository<T>)ProjectRepository;

            if (typeof(T).Name == typeof(Task).Name)
                return (IRepository<T>)TaskRepository;

            if (typeof(T).Name == typeof(TaskState).Name)
                return (IRepository<T>)TaskStateRepository;

            if (typeof(T).Name == typeof(Team).Name)
                return (IRepository<T>)TeamRepository;

            if (typeof(T).Name == typeof(User).Name)
                return (IRepository<T>)UserRepository;

            throw new NotImplementedException("Repository for " + nameof(T) + " not exist!");
        }
        #endregion
        #region Dispose
        public void Dispose()
        {
            projectRepository = null;
            taskRepository = null;
            taskStateRepository = null;
            teamRepository = null;
            userRepository = null;
        }
        #endregion
    }
}
