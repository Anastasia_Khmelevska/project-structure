﻿using LINQ.BLL.Entities;
using System;
using System.Collections.Generic;

namespace LINQ.DataAccess.Repositories
{
    public class UserRepository : Repository<User>
    {
        public UserRepository()
        {
            Entities = new List<User>();
            Entities.Add(new User
            {
                Id = 1,
                First_Name = "John1",
                Last_Name = "Doe1",
                Email = "john1.doe1@mail.com",
                Birthday = new DateTime(1990, 1, 1),
                Registered_At = new DateTime(2001, 9, 11),
                Team_Id = 1
            });
            Entities.Add(new User
            {
                Id = 2,
                First_Name = "John2",
                Last_Name = "Doe2",
                Email = "john2.doe2@mail.com",
                Birthday = new DateTime(1990, 1, 1),
                Registered_At = new DateTime(2001, 9, 11),
                Team_Id = 2
            });
            Entities.Add(new User
            {
                Id = 3,
                First_Name = "John3",
                Last_Name = "Doe3",
                Email = "john3.doe3@mail.com",
                Birthday = new DateTime(1990, 1, 1),
                Registered_At = new DateTime(2001, 9, 11),
                Team_Id = 3
            });
            Entities.Add(new User
            {
                Id = 4,
                First_Name = "John4",
                Last_Name = "Doe4",
                Email = "john4.doe4@mail.com",
                Birthday = new DateTime(1990, 1, 1),
                Registered_At = new DateTime(2001, 9, 11),
                Team_Id = 4
            });
            Entities.Add(new User
            {
                Id = 5,
                First_Name = "John5",
                Last_Name = "Doe5",
                Email = "john5.doe5@mail.com",
                Birthday = new DateTime(1990, 1, 1),
                Registered_At = new DateTime(2001, 9, 11),
                Team_Id = 5
            });
        }
    }
}
