﻿using System;
using System.Linq;
using System.Collections.Generic;
using LINQ.BLL.Entities;
using LINQ.BLL.Interfaces;

namespace LINQ.DataAccess.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : BaseEntity
    {
        #region Properties
        protected List<T> Entities;
        #endregion
        #region Public Methods
        public T Get(int id) =>
            Entities.SingleOrDefault(entity => entity.Id == id) ?? throw new NullReferenceException(nameof(T) + " entity with id " + id + " not exist!");
        public IEnumerable<T> GetAll() =>
            Entities;
        public T Add(T entity)
        {
            if (entity == null) throw new ArgumentNullException("Entity argument can't be bull!");
            Entities.Add(entity);
            return entity;
        }
        public T Update(int id, T Entity)
        {
            Delete(id);
            Entity.Id = id;
            Entities.Add(Entity);
            return Entity;
        }
        public void Delete(int id)
        {
            var _entity = Entities.SingleOrDefault(entity => entity.Id == id);
            if (_entity == null) throw new ArgumentException(nameof(T) + " entity with id " + id + " not found!");
            Entities.Remove(_entity);
        }
        #endregion
    }
}
