﻿using LINQ.BLL.Entities;
using System;
using System.Collections.Generic;

namespace LINQ.DataAccess.Repositories
{
    public class TaskRepository : Repository<Task>
    {
        public TaskRepository()
        {
            Entities = new List<Task>();
            Entities.Add(new Task
            {
                Id = 1,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 1,
                Project_Id = 1,
                Performer_Id = 1
            });
            Entities.Add(new Task
            {
                Id = 2,
                Name = "Name 2",
                Description = "Description 2",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 1,
                Project_Id = 2,
                Performer_Id = 1
            });
            Entities.Add(new Task
            {
                Id = 3,
                Name = "Name 3",
                Description = "Description 3",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 1,
                Project_Id = 3,
                Performer_Id = 1
            });
            Entities.Add(new Task
            {
                Id = 4,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 1,
                Project_Id = 4,
                Performer_Id = 1
            });
            Entities.Add(new Task
            {
                Id = 5,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 1,
                Project_Id = 5,
                Performer_Id = 2
            });
            Entities.Add(new Task
            {
                Id = 6,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 1,
                Project_Id = 6,
                Performer_Id = 2
            });
            Entities.Add(new Task
            {
                Id = 7,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 1,
                Project_Id = 7,
                Performer_Id = 2
            });
            Entities.Add(new Task
            {
                Id = 8,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 2,
                Project_Id = 8,
                Performer_Id = 2
            });
            Entities.Add(new Task
            {
                Id = 9,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 2,
                Project_Id = 9,
                Performer_Id = 3
            });
            Entities.Add(new Task
            {
                Id = 10,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 2,
                Project_Id = 10,
                Performer_Id = 3
            });
            Entities.Add(new Task
            {
                Id = 11,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 2,
                Project_Id = 11,
                Performer_Id = 3
            });
            Entities.Add(new Task
            {
                Id = 12,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 2,
                Project_Id = 12,
                Performer_Id = 3
            });
            Entities.Add(new Task
            {
                Id = 13,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 2,
                Project_Id = 13,
                Performer_Id = 4
            });
            Entities.Add(new Task
            {
                Id = 14,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 2,
                Project_Id = 14,
                Performer_Id = 4
            });
            Entities.Add(new Task
            {
                Id = 15,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 3,
                Project_Id = 15,
                Performer_Id = 4
            });
            Entities.Add(new Task
            {
                Id = 16,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 3,
                Project_Id = 16,
                Performer_Id = 4
            });
            Entities.Add(new Task
            {
                Id = 17,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 3,
                Project_Id = 17,
                Performer_Id = 5
            });
            Entities.Add(new Task
            {
                Id = 18,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 3,
                Project_Id = 18,
                Performer_Id = 5
            });
            Entities.Add(new Task
            {
                Id = 19,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 3,
                Project_Id = 19,
                Performer_Id = 5
            });
            Entities.Add(new Task
            {
                Id = 20,
                Name = "Name 20",
                Description = "Description 20",
                Created_At = new DateTime(2001, 2, 30),
                Finished_At = new DateTime(2002, 3, 23),
                State = 3,
                Project_Id = 20,
                Performer_Id = 5
            });
        }
    }
}
