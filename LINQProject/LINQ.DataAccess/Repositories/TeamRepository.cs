﻿using LINQ.BLL.Entities;
using System;
using System.Collections.Generic;

namespace LINQ.DataAccess.Repositories
{
    public class TeamRepository : Repository<Team>
    {
        public TeamRepository()
        {
            Entities = new List<Team>();
            Entities.Add(new Team
            {
                Id = 1,
                Name = "Name 1",
                Created_At = new DateTime(2006, 12, 3)
            });
            Entities.Add(new Team
            {
                Id = 2,
                Name = "Name 2",
                Created_At = new DateTime(2006, 12, 3)
            });
            Entities.Add(new Team
            {
                Id = 3,
                Name = "Name 3",
                Created_At = new DateTime(2006, 12, 3)
            });
            Entities.Add(new Team
            {
                Id = 4,
                Name = "Name 4",
                Created_At = new DateTime(2006, 12, 3)
            });
            Entities.Add(new Team
            {
                Id = 5,
                Name = "Name 5",
                Created_At = new DateTime(2006, 12, 3)
            });
            Entities.Add(new Team
            {
                Id = 6,
                Name = "Name 6",
                Created_At = new DateTime(2006, 12, 3)
            });
            Entities.Add(new Team
            {
                Id = 7,
                Name = "Name 7",
                Created_At = new DateTime(2006, 12, 3)
            });
            Entities.Add(new Team
            {
                Id = 8,
                Name = "Name 8",
                Created_At = new DateTime(2006, 12, 3)
            });
            Entities.Add(new Team
            {
                Id = 9,
                Name = "Name 9",
                Created_At = new DateTime(2006, 12, 3)
            });
            Entities.Add(new Team
            {
                Id = 10,
                Name = "Name 10",
                Created_At = new DateTime(2006, 12, 3)
            });
        }
    }
}
