﻿using System;
using System.Collections.Generic;
using LINQ.BLL.Entities;

namespace LINQ.DataAccess.Repositories
{
    public class ProjectRepository : Repository<Project>
    {
        public ProjectRepository()
        {
            Entities = new List<Project>();
            Entities.Add(new Project
            {
                Id = 1,
                Name = "Name 1",
                Description = "Description 1",
                Created_At = new DateTime(2001, 2, 2),
                Deadline = new DateTime(2002, 4, 2),
                Author_Id = 1,
                Team_Id = 1
            });
            Entities.Add(new Project
            {
                Id = 2,
                Name = "Name 2",
                Description = "Description 2",
                Created_At = new DateTime(2001, 1, 17),
                Deadline = new DateTime(2002, 1, 17),
                Author_Id = 1,
                Team_Id = 1
            });
            Entities.Add(new Project
            {
                Id = 3,
                Name = "Name 3",
                Description = "Description 3",
                Created_At = new DateTime(2002, 4, 21),
                Deadline = new DateTime(2003, 4, 21),
                Author_Id = 1,
                Team_Id = 2
            });
            Entities.Add(new Project
            {
                Id = 4,
                Name = "Name 4",
                Description = "Description 4",
                Created_At = new DateTime(2002, 5, 11),
                Deadline = new DateTime(2003, 5, 11),
                Author_Id = 1,
                Team_Id = 2
            });
            Entities.Add(new Project
            {
                Id = 5,
                Name = "Name 5",
                Description = "Description 5",
                Created_At = new DateTime(2003, 6, 26),
                Deadline = new DateTime(2004, 6, 26),
                Author_Id = 2,
                Team_Id = 3
            });
            Entities.Add(new Project
            {
                Id = 6,
                Name = "Name 6",
                Description = "Description 6",
                Created_At = new DateTime(2003, 3, 17),
                Deadline = new DateTime(2004, 3, 17),
                Author_Id = 2,
                Team_Id = 3
            });
            Entities.Add(new Project
            {
                Id = 7,
                Name = "Name 7",
                Description = "Description 7",
                Created_At = new DateTime(2004, 6, 15),
                Deadline = new DateTime(2005, 6, 15),
                Author_Id = 2,
                Team_Id = 4
            });
            Entities.Add(new Project
            {
                Id = 8,
                Name = "Name 8",
                Description = "Description 8",
                Created_At = new DateTime(2004, 10, 20),
                Deadline = new DateTime(2005, 10, 20),
                Author_Id = 2,
                Team_Id = 4
            });
            Entities.Add(new Project
            {
                Id = 9,
                Name = "Name 9",
                Description = "Description 9",
                Created_At = new DateTime(2005, 3, 9),
                Deadline = new DateTime(2006, 3, 9),
                Author_Id = 3,
                Team_Id = 5
            });
            Entities.Add(new Project
            {
                Id = 10,
                Name = "Name 10",
                Description = "Description 10",
                Created_At = new DateTime(2005, 7, 16),
                Deadline = new DateTime(2006, 7, 16),
                Author_Id = 3,
                Team_Id = 5
            });
            Entities.Add(new Project
            {
                Id = 11,
                Name = "Name 11",
                Description = "Description 11",
                Created_At = new DateTime(2006, 9, 17),
                Deadline = new DateTime(2007, 9, 17),
                Author_Id = 3,
                Team_Id = 6
            });
            Entities.Add(new Project
            {
                Id = 12,
                Name = "Name 12",
                Description = "Description 12",
                Created_At = new DateTime(2006, 11, 4),
                Deadline = new DateTime(2007, 11, 4),
                Author_Id = 3,
                Team_Id = 6
            });
            Entities.Add(new Project
            {
                Id = 13,
                Name = "Name 13",
                Description = "Description 13",
                Created_At = new DateTime(2007, 2, 2),
                Deadline = new DateTime(2008, 2, 2),
                Author_Id = 4,
                Team_Id = 7
            });
            Entities.Add(new Project
            {
                Id = 14,
                Name = "Name 14",
                Description = "Description 14",
                Created_At = new DateTime(2007, 1, 8),
                Deadline = new DateTime(2008, 1, 8),
                Author_Id = 4,
                Team_Id = 7
            });
            Entities.Add(new Project
            {
                Id = 15,
                Name = "Name 15",
                Description = "Description 15",
                Created_At = new DateTime(2008, 3, 23),
                Deadline = new DateTime(2009, 3, 23),
                Author_Id = 4,
                Team_Id = 8
            });
            Entities.Add(new Project
            {
                Id = 16,
                Name = "Name 16",
                Description = "Description 16",
                Created_At = new DateTime(2008, 7, 2),
                Deadline = new DateTime(2009, 7, 2),
                Author_Id = 4,
                Team_Id = 8
            });
            Entities.Add(new Project
            {
                Id = 17,
                Name = "Name 17",
                Description = "Description 17",
                Created_At = new DateTime(2009, 3, 12),
                Deadline = new DateTime(2010, 3, 12),
                Author_Id = 5,
                Team_Id = 9
            });
            Entities.Add(new Project
            {
                Id = 18,
                Name = "Name 18",
                Description = "Description 18",
                Created_At = new DateTime(2009, 7, 29),
                Deadline = new DateTime(2010, 7, 29),
                Author_Id = 5,
                Team_Id = 9
            });
            Entities.Add(new Project
            {
                Id = 19,
                Name = "Name 19",
                Description = "Description 19",
                Created_At = new DateTime(2010, 6, 2),
                Deadline = new DateTime(2011, 6, 2),
                Author_Id = 5,
                Team_Id = 10
            });
            Entities.Add(new Project
            {
                Id = 20,
                Name = "Name 20",
                Description = "Description 20",
                Created_At = new DateTime(2010, 12, 9),
                Deadline = new DateTime(2011, 12, 9),
                Author_Id = 5,
                Team_Id = 10
            });
        }
    }
}
