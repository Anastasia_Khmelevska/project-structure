﻿using LINQ.BLL.Entities;
using System.Collections.Generic;

namespace LINQ.DataAccess.Repositories
{
    public class TaskStateRepository : Repository<TaskState>
    {
        public TaskStateRepository()
        {
            Entities = new List<TaskState>();
            Entities.Add(new TaskState
            {
                Id = 1,
                Value = "Open"
            });
            Entities.Add(new TaskState
            {
                Id = 2,
                Value = "Started"
            });
            Entities.Add(new TaskState
            {
                Id = 3,
                Value = "Closed"
            });
        }
    }
}
